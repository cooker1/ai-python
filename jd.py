#!/usr/bin/python
# -*- coding: UTF-8 -*-

## 京东签到

import requests
import sys
import os

env = os.getenv("ENV")
if env == None or len(env) == 0:
    print("环境名为空")
    sys.exit()
elif "JD" != env:
    print("非JD脚本")
    sys.exit()


resp = requests.get("https://sourcegraph.com/github.com/NobyDa/Script@master/-/raw/JD-DailyBonus/JD_DailyBonus.js")
with open("jd.js", "wb") as code:
     code.write(resp.content)

cookie = os.getenv('JD_COOKIE');
if cookie == None or len(cookie) == 0:
    print("cookie 为空")
    sys.exit()

# var Key = '';

f = open('jd.js', 'r', encoding='utf-8')

f2 = open('jd-new.js', 'w', encoding='utf-8')

for line in f:
     if "var Key = ''; //单引号内自行填写您抓取的Cookie" in line:
          line = line.replace("''", "'" + cookie +"'")
     f2.write(line)

f.close()
f2.close()

os.renames("jd-new.js", "jd.js")
a
jdout = os.system("node jd.js")
print(jdout)
